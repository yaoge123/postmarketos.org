title: "D-Bus update preventing Phosh from running"
date: 2021-11-17
---

### Update

This issue had been resolved in `dbus-1.12.20-r4`

### Original post

A recent D-Bus update to `1.12.20-r3` in Alpine Linux is preventing Phosh from
running, the following message is seen in `~/.local/state/tinydm.log`:

```
(phoc:3061): phoc-wlroots-CRITICAL **: 21:06:25.913: [backend/session/logind.c:805] Failed to open D-Bus connection: Permission denied
```

If you have **not** already upgraded to `dbus-1.12.20-r3`, hold off on doing so at this time.

This post will be updated when a reliable workaround and/or fix is identified.

Also see:

- [aports!27527](https://gitlab.alpinelinux.org/alpine/aports/-/merge_requests/27527)

- [aports#13204](https://gitlab.alpinelinux.org/alpine/aports/-/issues/13204)
