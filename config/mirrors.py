# Copyright 2022 Oliver Smith
# SPDX-License-Identifier: AGPL-3.0-or-later

mirrors = {
        "mirror.postmarketos.org": {
            "urls": [
                "http://mirror.postmarketos.org/postmarketos/",
                "https://mirror.postmarketos.org/postmarketos/",
                "rsync://mirror.postmarketos.org::postmarketos",
            ],
            "location": "Falkenstein, Germany",
            "bandwidth": "1 Gbps",
        },
        "postmarketos.craftyguy.net": {
            "urls": [
                "http://postmarketos.craftyguy.net/",
            ],
            "location": "Santa Clara, CA, USA",
            "bandwidth": "750 Mbps",
        },
        "southeastasia.cloudapp.azure.com": {
            "urls": [
                "http://postmarketos-mirror.southeastasia.cloudapp.azure.com/",
            ],
            "location": "Singapore",
        },
        "eastus2.cloudapp.azure.com": {
            "urls": [
                "http://postmarketos-mirror.eastus2.cloudapp.azure.com/",
            ],
            "location": "Virginia, USA",
        },
        "southafricanorth.cloudapp.azure.com": {
            "urls": [
		"http://postmarketos-mirror.southafricanorth.cloudapp.azure.com/",
            ],
            "location": "Johannesburg, Africa",
        },
        "mirror.math.princeton.edu": {
            "urls": [
                "http://mirror.math.princeton.edu/pub/postmarketos/",
                "https://mirror.math.princeton.edu/pub/postmarketos/",
                "rsync://mirror.math.princeton.edu/pub/postmarketos",
            ],
            "location": "Princeton, USA",
            "bandwidth": "4x10 Gbps",
        },
        "mirrors.aliyun.com": {
            "urls": [
                "https://mirrors.aliyun.com/postmarketOS/",
                "http://mirrors.aliyun.com/postmarketOS/",
            ],
            "location": "Hangzhou, China",
            "bandwidth": "10 Gbps",
        },
        "mirrors.tuna.tsinghua.edu.cn": {
            "urls": [
                "https://mirrors.tuna.tsinghua.edu.cn/postmarketOS/",
                "http://mirrors.tuna.tsinghua.edu.cn/postmarketOS/",
                "rsync://mirrors.tuna.tsinghua.edu.cn/postmarketOS",
            ],
            "location": "Beijing, China",
            "bandwidth": "10 Gbps",
        },
        "mirrors.bfsu.edu.cn": {
            "urls": [
                "https://mirrors.bfsu.edu.cn/postmarketOS/",
                "http://mirrors.bfsu.edu.cn/postmarketOS/",
                "rsync://mirrors.bfsu.edu.cn/postmarketOS",
            ],
            "location": "Beijing, China",
            "bandwidth": "1 Gbps",
        },
        "mirrors.ustc.edu.cn": {
            "urls": [
                "https://mirrors.ustc.edu.cn/postmarketos/",
                "http://mirrors.ustc.edu.cn/postmarketos/"
            ],
            "location": "Anhui, China",
            "bandwidth": "10 Gbps",
        },
        "mirror.nju.edu.cn": {
            "urls": [
                "https://mirror.nju.edu.cn/postmarketos/",
                "http://mirror.nju.edu.cn/postmarketos/",
                "rsync://mirror.nju.edu.cn/postmarketos",
            ],
            "location": "Nanjing, Jiangsu, China",
            "bandwidth": "10 Gbps",
        },
    }
